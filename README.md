
# README
This repository contains detailed results for the experimental section in the paper
**Reinforcement Learning Approach for Search Tree Size Minimization in Constraint Programming: New Results on Scheduling Benchmarks**.

There are 3 folders, each containing results denoted by the Table <id> used in the paper. Results are in .json format for each instance of the tested dataset.


The results were obtained using the Optal solver of version 0.8.5, so the results produced by other versions of the Optal solver might be slightly different. 

The previous state-of-the-art results for JSSP can be found across these three websites:
- https://github.com/thomasWeise/jsspInstancesAndResults/tree/master#PLC,
- http://jobshop.jjvh.nl/,
- https://optimizizer.com/TA.php,

while state-of-the-art results for RCPSP instance can be found across these two websites:
- https://www.om-db.wi.tum.de/psplib/getdata_sm.html
- https://www.projectmanagement.ugent.be/research/project_scheduling/rcpsp